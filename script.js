const notAllowed = 'You are not allowed to visit this website';

let name = prompt('Enter your name', '');
while (name == '' || isFinite(Number(name))) {
    name = prompt('Enter your name', '');
}

let age = +prompt('Enter your age', '');
while (!isFinite(age)) {
    age = +prompt('Enter your age', '');

}

if (age < 18) {
    alert(notAllowed);
} else if (18 <= age && age <= 22) {
    let conf = confirm('Are you sure you want to continue?');
    if (conf) {
        alert(`Welcome, ${name}`);
    } else {
        alert(notAllowed);
    }
} else {
    alert(`Welcome, ${name}`);
}











